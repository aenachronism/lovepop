
# Quick Start

This project is built and tested with [sbt-1.0](http://www.scala-sbt.org/download.html) on Mac OSX.  

It is unclear that it will work with other versions of sbt, or on different operating systems (I only have access to Macs).

1. [Download](http://www.scala-sbt.org/download.html) and install sbt as appropriate for you operating system.
2. Clone this repository.
3. Change to the directory where you cloned the repo in step 2.
4. Run the command : `sbt "run --out sample.copy.svg sample.svg"` Note the quotes `"` in the command, they are significant.
5. `sbt` logging is very chatty, and you will see a number of messages relating to downloading dependencies and compilation, but eventually, the application will start prompting for replacement values for fill and transform attributes (e.g. `fill=[#CE2334]:`)
6. Specify the new values for attributes ( hitting enter accepts the previous value, leaving it unmodified )
7. After all the new attribute values have been specified, the resulting `svg` file is written to the console and to a new file specified by the `--out` flag to the `sbt` command.

# Sample output

The sample below elides the logging output produced by `sbt`

```css
fill=[#CE2334]: #ff0000
fill=[#CE2334]: #00ff00
fill=[#CE2334]: #0000ff
fill=[#CE2334]: #ff0000
fill=[#CF2435]:
transform=[rotate(5 5 5)]: rotate(20)
<svg xml:space="preserve" enable-background="new 0 0 216 216" viewBox="0 0 216 216" y="0px" x="0px" id="Layer_1" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
<path fill="#ff0000" d="M89.3,150.2c0.3-2,1.2-3.7,2.6-5c1.4-1.3,3.1-1.9,5.1-1.9c2.2,0,4,0.7,5.5,2.1c1.5,1.4,2.4,3,2.7,4.8H89.3z M105.9,143c-2.7-2.4-5.8-3.6-9.2-3.6c-3.4,0-6.5,1.2-9.1,3.7c-2.7,2.5-4,5.7-4,9.5c0,4.1,1.4,7.3,4.1,9.7c2.7,2.4,6,3.6,9.8,3.6c2.5,0,4.8-0.5,7.1-1.6c2.3-1.1,4.1-2.7,5.5-4.9l-4.4-1.9c-2,2.8-4.7,4.1-8.2,4.1c-2.4,0-4.3-0.7-5.8-2.1c-1.5-1.4-2.4-3.3-2.7-5.6 h21.1C110.1,149,108.7,145.4,105.9,143 M73.2,165.5l8-21.1h3.9V140H72.8v4.4h3.5l-5.4,14.2l-5.4-14.2h3.3V140H56.1v4.4h4.4l8.2,21.1 H73.2z M38.5,146.5c1.5-1.6,3.4-2.4,5.7-2.4c2.2,0,4.1,0.8,5.6,2.4c1.6,1.6,2.3,3.6,2.3,6.1c0,2.4-0.8,4.5-2.3,6.1c-1.5,1.6-3.4,2.4-5.7,2.4c-2.2,0-4.1-0.8-5.7-2.4c-1.5-1.6-2.3-3.6-2.3-6.1C36.3,150.2,37,148.2,38.5,146.5 M34.9,162.1c2.5,2.5,5.7,3.7,9.4,3.7c3.6,0,6.7-1.2,9.3-3.7c2.6-2.5,3.9-5.6,3.9-9.6c0-3.9-1.3-7.1-3.9-9.6c-2.6-2.4-5.7-3.7-9.3-3.7c-3.7,0-6.8,1.2-9.4,3.7c-2.6,2.5-3.8,5.7-3.8,9.5C31.1,156.4,32.3,159.6,34.9,162.1 M17.9,160.7v4.4h13.2v-4.4h-3.8v-31.7h-9.4v4.4 h4.5v27.3H17.9z"/>
<path fill="#00ff00" d="M150.4,157.6c-0.8-1.3-1.2-2.9-1.2-4.8c0-4.8,1.6-7.2,4.9-7.2c1.5,0,2.8,0.6,3.7,1.8c0.9,1.2,1.3,2.9,1.3,5c0,2.4-0.5,4.2-1.4,5.4c-0.9,1.1-2.1,1.7-3.6,1.7C152.4,159.5,151.2,158.8,150.4,157.6M163.5,162.1c2.5-2.4,3.8-5.6,3.8-9.6c0-4.1-1.3-7.3-3.8-9.6c-2.5-2.3-5.7-3.5-9.4-3.5c-3.7,0-6.8,1.2-9.4,3.5c-2.5,2.3-3.8,5.5-3.8,9.6c0,4.1,1.3,7.3,3.8,9.6c2.5,2.3,5.7,3.5,9.4,3.5C157.9,165.7,161,164.5,163.5,162.1M130.7,157.7c-0.9,1.1-2.1,1.6-3.5,1.6c-1.6,0-2.8-0.6-3.7-1.8c-0.9-1.2-1.4-2.8-1.4-4.8c0-2.2,0.5-3.9,1.4-5c0.9-1.1,2.2-1.7,3.7-1.7c1.5,0,2.7,0.5,3.6,1.6c0.9,1.1,1.4,2.7,1.4,4.9C132.1,154.9,131.6,156.6,130.7,157.7M122.1,170.7v-8.1c2.2,2.1,4.7,3.1,7.3,3.1c3.3,0,5.9-1.2,7.9-3.7c2-2.4,3-5.5,3-9.2c0-3.5-1-6.6-2.9-9.2c-1.9-2.6-4.5-4-7.8-4c-2.7,0-5.2,1.2-7.5,3.5V140h-11.5v5.8h3.5v24.8h-4.2v5.8h17.3v-5.8H122.1z"/>
<path fill="#0000ff" d="M187.8,157.7c-0.9,1.1-2.1,1.6-3.5,1.6c-1.6,0-2.8-0.6-3.7-1.8c-0.9-1.2-1.4-2.8-1.4-4.8c0-2.2,0.5-3.9,1.4-5c0.9-1.1,2.2-1.7,3.7-1.7c1.5,0,2.7,0.5,3.6,1.6c0.9,1.1,1.4,2.7,1.4,4.9C189.2,154.9,188.7,156.6,187.8,157.7M179.2,170.7v-8.1c2.2,2.1,4.7,3.1,7.3,3.1c3.3,0,5.9-1.2,7.9-3.7c2-2.4,3-5.5,3-9.2c0-3.5-1-6.6-2.9-9.2c-1.9-2.6-4.5-4-7.8-4c-2.7,0-5.2,1.2-7.5,3.5V140h-11.5v5.8h3.5v24.8H167v5.8h17.3v-5.8H179.2z"/>
<path fill="#ff0000" d="M98.4,74.8c4.6,0.7,7.6,2.9,9.4,6.4c1.8-3.5,4.9-5.8,9.4-6.4c0.9,0,1.8,0,2.6,0c5.9,1.2,9.3,4.7,10.2,10.7c0,1-0.1,2-0.1,2.9c-0.6,4.1-1.6,6.1-3.6,8.2l-18.6,18.4L89.3,96.7c-2-2.1-3.1-4.1-3.6-8.2c0-1-0.1-2-0.1-2.9 c0.9-6,4.3-9.6,10.2-10.7C96.7,74.8,97.5,74.8,98.4,74.8M75.7,40.6L56.4,82.1l51.5,51.5l51.5-51.5l-19.3-41.5l-32.1,32.1c0,0-0.1,0-0.1,0c0,0,0,0-0.1,0L75.7,40.6z"/>
<text transform="rotate(20)" fill="#CF2435" y="180" x="50"><tspan>engineering</tspan></text>
</svg>
```  

Note that there are four path and one text element with fill attributes for a total of five elements.  The text element also specifies a transformation via rotation.
  
# Discussion

Scala provides direct support for `XML` as part of the language.  This means that, among other things, you can include `XML` literals directly in the code:

```Scala
val anchor = <a href="https://www.w3schools.com">Visit W3Schools.com!</a>
```

Note that `anchor` above is of type `scala.xml.Elem`, not `String` and the type was constructed on our behalf by the compiler.

As a result of this built in support for the language, we are afforded some nice syntactic constructs for working with XML.

For example, we get XPath like queries where we can get all of the children of a given node, or all of its descendants.  So, to find all of the `<img>` nodes in a document, one can simply write `doc \\ "img"` (assuming that `doc` is the root element of the DOM tree)

We are also provided with methods to transform `XML` simply by providing implementations of the abstract class `RewriteRule` that define the function `def transform(n: Node): Seq[Node]`.  These rewrite rules are then provided to a `RuleTransformer` which traverses the DOM and modifies nodes in accordance with the `RewriteRule`s.

Unfortunately, there seems to be a bug with the implementation of the `RuleTransformer` which repeatedly revisits a node that has a modified copy.  This means that modifying a fill attribute results in the node with the previous fill being presented for update ad infinitum as long as one keeps changing the fill value. You can observe this behaviour by passing the `--borked` flag to the `sbt` command: `sbt --borked --out new-sample.svg sample.svg`

Running the code with the `--borked` flag and supplying different values for the fill attributes results in the user being prompted many more times for fill values than expected four, as indicated in the sample output.

In order to work around this behaviour, I also provided a second, correct, implementation that explicitly traverses the `DOM` tree in a depth first manner and modifies any nodes as appropriate:

The `xform` function (x for cross, where crossform ~= transform) takes care of the tree traversal, while the `update` function is responsible for modifying the individual nodes.

```scala
  /**
    * Traverse the tree in a depth first manner, modifying nodes as appropriate.  Because xml nodes are immutable in
    * Scala, modification implies construction of a new node with the required updates.  Since it is possible that we
    * will modify leaf nodes, we need to build our tree from the bottom up (i.e. we first modify the children which we
    * then add to the parent )
    * @param n The root node of the tree to transform
    * @return A new tree with the applied transformations
    */
  def xform(n: Node): Node = n match {
    case e: Elem if (e.child.nonEmpty) => update(e).copy(child = e.child map xform)
    case e: Elem                       => update(e)
    case _                             => n
  }
```  

The resulting code is larger than it needs to be, since it also contains the malfunctioning implementation, but I thought it was instructional to be able to run the two implementatoins back to back.  Both implementations, however, share the underlying code that modifies nodes, so the code duplication is kept to a minimum.  This also helps to illustrate the fact that the source of the errant behaviour seems to be a result of the DOM traversal. 

The bulk of the code is located in `[root]/lovepop/src/main/scala/com/lovepop/svg/Main.scala`.  There is an additional file that is used to define and parse command line arguments.

