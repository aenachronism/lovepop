import sbt.util

scalaVersion := "2.12.3"
name         := "transform svg"
organization := "com.lovepop"
version      := "1.0"

logLevel in run := Level.Info

libraryDependencies += "org.scala-lang.modules" %% "scala-xml"     % "1.0.5"
libraryDependencies += "com.github.scopt"       %% "scopt"         % "3.7.0"
libraryDependencies += "org.wvlet.airframe"      % "airframe_2.12" % "0.37"


