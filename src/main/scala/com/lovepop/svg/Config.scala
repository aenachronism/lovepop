package com.lovepop.svg

import java.io.File

import scopt.OptionParser

object Config {
  val parser = new OptionParser[Config]("xform") {
    head("xform", "0.1")

    opt[String]('o', "out").
      valueName("<file>").
      action((x: String, c: Config) => c.copy(out = Option(x))).
      text("The name of the output file.")

    opt[Unit]('b', "borked").
      action((_, c) => c.copy(borked = true)).
      text("Use the broken implementation.")

    help("help").text("prints this usage text")

    arg[String]("<file>").
      required.
      action((x, c) => c.copy(file = x)).
      text("The input file to convert.")
  }
}

case class Config(file: String = "", out: Option[String] = None, borked: Boolean = false)
