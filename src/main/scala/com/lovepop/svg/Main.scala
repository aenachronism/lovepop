package com.lovepop.svg

import scala.xml._
import scala.io.StdIn
import scala.xml.transform.{RewriteRule, RuleTransformer}
import wvlet.log.LogSupport

object Main extends LogSupport {
  type Prompt  = String
  type Default = String

  /**
    * Prompts the user for input, allowing them to hit enter to accept the previous value
    *
    * todo: We need to validate user input, since users are error prone if not outright malicious.  This is
    *       tricky, however, because user input validity is context dependent ( we need to know what attributes
    *       we are modifying, and what the grammar tells us is permissible ).  This is left as an exercise for
    *       the reader ;)
    */
  val promptUser: (Prompt, Default) => String = (p, d) => {
    val result = StdIn.readLine(s"$p=[$d]: ")
    if (result.isEmpty) d else result
  }

  /**
    * Transforms the fill color of a node
    */
  val fillXform = new RewriteRule {
    /**
      * @param n The node to transform
      * @return A sequence of nodes with the appropriate modifications. Note that this is a one to many transformation
      *         in order to maintain maximum generality. However, in our case, we are only manipulating attributes of a
      *         node which means that we will always return a sequence of only one node.  So when we are finished, we
      *         will need to take the first node of the sequence to get at the actual result.
      */
    override def transform(n: Node): Seq[Node] = n match {
      case e:Elem  => updateFill(e)
      case _       => n
    }
  }

  /**
    * Transform the rotate value of a the "transform" attribute of a node.
    */
  val transformXform = new RewriteRule {
    override def transform(n: Node): Seq[Node] = n match {
      case e: Elem => updateTransform(e)
      case _       => n
    }
  }

  type Transformation = Elem => Elem

  /**
    * Update the fill attribute
    */
  val updateFill: Transformation = e => {
    if (e \@ "fill" != "") {
      val fill = promptUser("fill", e \@ "fill")
      e % Attribute(null, "fill", fill , Null)
    } else e
  }

  /**
    * Update the transform attribute if it is a rotation
    */
  val updateTransform: Transformation = e => {
    if ((e \@ "transform").startsWith("rotate(")) {
      val rotate = promptUser("transform", e \@ "transform")
      e % Attribute(null, "transform", rotate, Null)
    } else e
  }

  def update = updateFill andThen updateTransform

  /**
    * Traverse the tree in a depth first manner, modifying nodes as appropriate.  Because xml nodes are immutable in
    * Scala, modification implies construction of a new node with the required updates.  Since it is possible that we
    * will modify leaf nodes, we need to build our tree from the bottom up (i.e. we first modify the children which we
    * then add to the parent )
    * @param n The root node of the tree to transform
    * @return A new tree with the applied transformations
    */
  def xform(n: Node): Node = n match {
    case e: Elem if (e.child.nonEmpty) => update(e).copy(child = e.child map xform)
    case e: Elem                       => update(e)
    case _                             => n
  }

  def main(args: Array[String]): Unit = {
    Config.parser.parse(args, Config()).foreach { cfg =>
      val root: Elem  = XML.loadFile(cfg.file)
      val out         = cfg.out.getOrElse(s"${cfg.file}.copy")
      val result      =
        if (!cfg.borked) xform(root)
        else new RuleTransformer(fillXform, transformXform).transform(root)(0)

      info(s"Converted :${cfg.file}")

      XML.save(out, result)

      info(s"Result [${out}]: \n\t\t${result}")
    }
  }
}